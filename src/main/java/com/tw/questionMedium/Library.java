package com.tw.questionMedium;

import java.util.*;

public class Library {
    private final List<Book> books = new ArrayList<>();

    public void addAll(Book... books) {
        this.books.addAll(Arrays.asList(books));
    }

    /**
     * Find all the books which contains one of the specified tags.
     *
     * @param tags The tag list.
     * @return The books which contains one of the specified tags. The returned books
     * should be ordered by their ISBN number.
     */
    public List<Book> findBooksByTag(String... tags) {
        // TODO:
        //   Please implement the method
        // <-start-
        if (tags.length == 0) return new ArrayList<Book>();

        List<Book> requiredBooks = new ArrayList<Book>();
        Boolean addBook = true;

        for (int i = 0; i < tags.length; i++) {
            for (Book book : books) {
                for (String tag : book.getTags()) {
                    if (tags[i].equals(tag)) {
                        for (int j = 0; j < requiredBooks.size(); j++) {
                            if (requiredBooks.get(j).getName().equals(book.getName())) {
                                addBook = false;
                            }
                        }
                        if (addBook)
                            requiredBooks.add(book);
                    }
                }
            }
        }

        Collections.sort(requiredBooks, new Comparator<Book>() {
            public int compare(Book book1, Book book2) {
                if (Long.parseLong(book1.getIsbn()) - Long.parseLong(book2.getIsbn()) < 0) return -1;
                if (Long.parseLong(book1.getIsbn()) - Long.parseLong(book2.getIsbn()) > 0) return 1;
                else return 0;
            }
        });

        return requiredBooks;
        // --end-->
    }

    // TODO:
    //   You can add additional methods here if you want.
    // <-start-

    // --end-->
}
